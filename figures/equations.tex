\documentclass[acmsmall,screen,review]{acmart}
\usepackage{balance}
\usepackage{prftree}
\usepackage{syntax}
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage{subcaption}
\usepackage[most]{tcolorbox}

\tcbset{
    boxmath/.style 2 args={%
        enhanced, colback=white,
        colframe=#1, remember as=#2, size=fbox}
    }

%TODO: think about this
\usepackage{anyfontsize}

\citestyle{acmauthoryear}

\renewcommand{\syntleft}{\normalfont\itshape}
\renewcommand{\syntright}{\normalfont\itshape}

\title{Explicit Refinement Types}
%Or, Seizing the Means of Verification
\author{Jad Elkhaleq Ghalayini}
\affiliation{%
    \institution{University of Cambridge}
    \department{Department of Computer Science and Technology}
    \streetaddress{William Gates Building}
    \city{Cambridge}
    \country{United Kingdom}
}
\email{jeg74@cl.cam.ac.uk}
\author{Neel Krishnaswami}
\affiliation{%
    \institution{University of Cambridge}
    \department{Department of Computer Science and Technology}
    \streetaddress{William Gates Building}
    \city{Cambridge}
    \country{United Kingdom}
}
\email{nk480@cl.cam.ac.uk}

\begin{CCSXML}
    <ccs2012>
    <concept>
    <concept_id>10003752.10010124.10010138.10010142</concept_id>
    <concept_desc>Theory of computation~Program verification</concept_desc>
    <concept_significance>500</concept_significance>
    </concept>
    <concept>
    <concept_id>10003752.10010124.10010131.10010133</concept_id>
    <concept_desc>Theory of computation~Denotational semantics</concept_desc>
    <concept_significance>500</concept_significance>
    </concept>
    </ccs2012>
\end{CCSXML}
    
\ccsdesc[500]{Theory of computation~Program verification}
\ccsdesc[500]{Theory of computation~Denotational semantics}

\keywords{
    Refinement Types, First Order Logic, Denotational Semantics
}

% Math fonts
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\mt}[1]{\mathtt{#1}}
\newcommand{\mrm}[1]{\mathrm{#1}}
\newcommand{\scs}[1]{{\scriptstyle #1}}
\newcommand{\mx}[1]{\mbox{#1}}
\newcommand{\ol}[1]{\overline{#1}}
\newcommand{\ul}[1]{\underline{#1}}
\newcommand{\ms}[1]{\mathsf{#1}}

% Text fonts
\newcommand{\tty}[1]{\texttt{#1}}
\newcommand{\rle}[1]{{\scriptsize\textsf{#1}}}

% Math symbols
\newcommand{\nats}{\mbb{N}}
\newcommand{\reals}{\mbb{R}}
\newcommand{\ints}{\mbb{Z}}
\newcommand{\rationals}{\mbb{Q}}

% ERT-specific notation
\newcommand{\lrset}[3]{\left\{{#1}: {#2} \mid {#3}\right\}}
\newcommand{\lrasm}[3]{{#1}: {#2} \Rightarrow {#3}}
\newcommand{\lrimp}[3]{{#1}: {#2} \Rightarrow {#3}}
\newcommand{\upg}[1]{{#1}^{\uparrow}}
\newcommand{\dng}[1]{{#1}^{\downarrow}}
\newcommand{\subctx}[2]{{#1} \leq {#2}}
\newcommand{\dnt}[1]{\llbracket{#1}\rrbracket}
\newcommand{\ers}[1]{|{#1}|}
\newcommand{\bers}[1]{\left|{#1}\right|}
\newcommand{\ednt}[1]{\left\llbracket{#1}\right\rrbracket}
\newcommand{\lrnt}{\vdash}
\newcommand{\stnt}{\vdash_{\lambda}}

% TODOs
\newcounter{todos}
\newcommand{\TODO}[1]{{
  \stepcounter{todos}
  \begin{center}\large{\textcolor{red}{\textbf{TODO \arabic{todos}:} #1}}\end{center}
}}
\newcommand{\sorry}{{
  \stepcounter{todos}
  \text{\textcolor{red}{\textbf{sorry \arabic{todos}}}}
}}

\begin{document}

\begin{equation}
    \dnt{A} \in \ms{Set} \qquad
    \dnt{\Gamma} \equiv \dnt{A} \times \dnt{B} \times ... \in \ms{Set} \qquad
    \dnt{\Gamma \vdash a: A}: \dnt{\Gamma} \to \dnt{A}
\end{equation}

\begin{equation}
    (A, P) \qquad
    f: (A, P) \to (B, Q) \qquad
    A \to B \qquad
    \forall x \in P, f(x) \in Q
\end{equation}

\begin{equation}
    A \to \ms{M}B \qquad
    \forall x \in P, f(x) \in \ms{ret}\;Q
\end{equation}

\begin{equation}
    \top \qquad
    \bot \qquad
    \top \implies \bot \qquad
    \top \implies \top
\end{equation}

\begin{equation}
    \tty{type NonEmpty A = \{l: List A | len l > 0\}}
\end{equation}

\begin{equation}
    \tty{head :: NonEmpty A \(\to\) A}
\end{equation}

\begin{equation}
    \tty{len}\;[] > 0 \qquad
    \bot \qquad
    \tty{len}\;[] > 0 \implies \bot \qquad
    \top \implies \top
\end{equation}

\begin{equation}
    \forall x: \nats, f(x) \neq 0
\end{equation}

\begin{equation}
    \forall x: \nats, \exists y: \nats, g(x, y) = 0
\end{equation}

\begin{equation}
    \forall x: \nats, f(x) \leq f(x + 1)
\end{equation}

\begin{equation}
    \tty{if}\;P(x_0, x_1, ..., x_n) = 0\;\tty{then undef else}\;1
\end{equation}

\begin{equation}
    P(x_0, x_1, ..., x_n) = \sum_ia_i\prod_jx_{ij}^{n_{ij}}
\end{equation}

\begin{equation}
    P(x, y, z) = x^2y + 3xzy^2 + 2x
\end{equation}

\begin{equation}
    3x + 6y + 8z \leq 4 \qquad
    xy \leq 4
\end{equation}

\begin{equation}
    a = -3/2, b = -1/2
\end{equation}

\begin{equation}
    f = g = \mb{false}
\end{equation}

\end{document}