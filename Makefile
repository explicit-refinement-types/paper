all: paper-revised.pdf paper-final.pdf

paper-revised.pdf: paper-revised.tex references.bib
	latexmk -pdf paper-revised.tex

paper-diff.pdf: paper-revised.tex paper.tex references.bib
	latexdiff paper.tex paper-revised.tex > paper-diff.tex
	latexmk -pdf paper-diff.pdf

paper-final.pdf: paper-final.tex
	latexmk -pdf paper-final.tex