OOPSLA 2023 Paper #32 Reviews and Comments
===========================================================================
Paper #32 Explicit Refinement Types


Review #32A
===========================================================================

Overall merit
-------------
C. Weak reject

Reviewer expertise
------------------
X. Expert

Paper summary
-------------
The paper presents a system for "Explicit Refinement Types", an
extension of refinement typing systems to include explicit proof
objects.

Assessment
----------
The goal of the work is to add explicit syntax for proof objects to a
refinement type system. I find that the work does an insufficient job
in demonstrating novelty over prior work in dependent type
theories. For this reason I will not argue to accept the paper.

Comments for authors
--------------------
, whereas typically the pragmatic difference
between refinement type systems and dependent type systems is that
refinement types use (intentionally) limited fragments of logic that
can be automatically discharged by solvers. Once we are dealing with
the proof objects explicitly, since the system is a pure lambda
calculus I have a hard time seeing why this system is preferred over
previously proposed and studied dependent type theories.

From my reading, the main arguments for novelty are (1) the use of
"ghost variables" to support a kind of phase distinction between
compile time and run-time and (2) the erasure based denotational
semantics. However there is extensive related work in the field of
dependent type theory already on erasure: Agda, Idris, Dependent
Haskell and Zombie all supported some version of this. For an
extensive account of related work in this area I recommend Matúš
Tejiščák's PhD dissertation or the associated ICFP paper. On the more
semantic side, recent work by Sterling & Harper (JACM 2021) defines a
dependent type theory with a phase distinction between runtime data
and static proof objects with a denotational semantics that is quite
similar to the one given in this paper (though presented very
differently in terms of category theory).

Questions for author response
-----------------------------
Which aspects of your system are you claiming to be novel compared to
prior work on dependent type theory such as those mentioned above?



Review #32B
===========================================================================

Overall merit
-------------
C. Weak reject

Reviewer expertise
------------------
X. Expert

Paper summary
-------------
This paper develops a new type theory (called lambda_ert) for
refinement types where proofs of propositions are treated explicitly
inside the type theory.  In addition to subset types (as is often
found in refinement type systems), the calculus supports proof
irrelevance and ghost variables, which do not affect the final outcome
of a program.  Unlike many dependent type theories, definitional
equality is not part of the calculus and every propositional equality
has to be proved explicitly using axioms that capture beta eta
equality.

The paper presents the formal definition of lambda_ert with its syntax
and type system.  Erasure to simply typed lambda calculus and the
logical predicate to interpret refinement types are given as the
semantics of lambda_ert.  Finally, it is stated that an erasure
satisfy the logical predicate, meaning that the erasure satisfies the
specification given as the refinement type of the original term.

Assessment
----------
Pros:

* Solid technical work, which captures some important aspects of refinement types.
* All the proofs of the theorems stated in the paper have been mechanized, by using Lean 4.
* Writing is fairly clear (although there are several typos).

Cons:

* Novelty of the work is not really clear.  

Although I enjoyed reading the paper, I'm left with a question "What's
really new in this paper?"  Explicit proofs for propositions in subset
types are a fairly traditional way to deal with subset types in type
theory; proof irrelevance has been studied before; and type theory
without defnitional equality has been found elsewhere.  It may be the
case that the particular combination of type-theoretic features in
lambda_ert hasn't been studied rigorously, but it is not easy to see
particular difficulties in combining.  (There may be some but, at
least, it's not clear from writing.)  So, even though the technical
part has been done elegantly and all the proofs are mechanized (both
of which I appreciate), I'm not sure if this work warrants
publication.

Comments for authors
--------------------
L150: I'm not sure it is standard to call universal and existential
quantified types intersection and union types (respectively).
(WONTFIX)

L187: "a proof [of] a proposition"
(FIXED)

L254: "let v = |\ell|" should be "let \ell = |v|"?
(FIXED)

L272: "likseo"
(FIXED)

L292: At this point, it is not clear that $\beta$ is a constant for an
axiom of beta-equality.
(FIXED)

L353: $\varphi$ and $\phi$ are confused.
(FIXED)

Fig. 5(b): Should "u" be "x" in the clauses for universal and
existential quantifiers?
(FIXED)

L518: "parametrized" -> "parameterized"  (I saw "parameterised" somewhere else.)
(FIXED)

L574: "valid a..", "to for"
(FIXED)


L577: "))"
(FIXED)

L630: "repretenting"
(FIXED)

Fig. 11: Many rules omit types as subscripts for =.
(FIXED)

Rule \eta_ty: Should the proof term be "\eta_{ty} f"?
(FIXED)

L731: Let-Wit doesn't exist.  Let-Exists?
(FIXED)

Fig. 17:
* \mathcal{E} should be explained in the body of the paper.
* The defining clause for \mathcal{E} (L1050) does not seem quite right:
I think "ret ..." should be "{ret x | x \in [| \Gamma \vdash_\lambda A ty |]}".
(FIXED)

* L1044: \mathcal{E} is missing after "f () \in"?
(FIXED)
* L1067: \Gamma \vdash x : A should be enclosed by | |?
(FIXED)

Related work on categorical semantics: Kura (FoSSaCS 2021) has given
a category-theoretic, generic account of refinement type systems.

(FIXED)

L1101: "as" -> "is"

(FIXED) 

L1140: More bibliographic innformation should be given to Norell 2007.

(FIXED) 

Questions for author response
-----------------------------
Can you clarify what's really new in lambda_ert?



Review #32C
===========================================================================

Overall merit
-------------
C. Weak reject

Reviewer expertise
------------------
X. Expert

Paper summary
-------------
This paper presents and develop a simply typed lambda-calculus called λert
extended with refinement types and explicit proofs terms for these
proof-irrelevant refinements. λert is proposed as a well-behaved yet expressive
core calculus for more complex systems with refinement types and liquid typing
system, putting aside the decidability and algorithmic aspects of proof
generation and verification often tackled with off-the-shelf smt solvers that
add another layer of complexity and orthogonal restrictions (e.g. for quantifier
instantiation). The authors design λert so that it admit a simple extraction
procedure to a simply-typed lambda-calculus with exceptions by erasing the
refinements. These extracted programs are then shown to satisfy the contracts
specified by their refinements using a denotational model. The authors
formalized mechanically their results in the Lean 4 proof assistant.

Assessment
----------
The paper is motivated and provides a gentle introduction to the topic, with an
adequate section explaining the challenges tackled by this paper. The results
contained in the paper do correspond to the claims in the introduction. The
content and results of the paper are however relatively expected, and sometimes
a bit convoluted with respect to similar ideas present in the literature: in
particular considering an explicit squash operator as in [1], and reusing the
work on modal lambda-calculi would have factored away a lot of the work
presented in this paper. At a superficial level, the paper suffer from many
typos and small mistakes (see below), some of them due to the complexity
introduced by the duplication of constructs between types and propositions (e.g.
between product of types and existential quantification, or universal
quantifications on types and propositions). The final expressivity of the system
is also not entirely clear: induction on natural numbers allows to do many
proofs of arithmetic statements, the absence of confusion between 0 and
successor is obtained in combination with axioms on sum types. Similarly,
equality is congruent (thanks to subst) but it does not support any form of
function extensionality (a.k.a. ξ-rule for definitional equality), so expressions
involving a bound variable cannot be rewritten.

The formalization seems robust but sometimes hard to navigate, in particular in
the lemmas that are proven by hand. The paper does not provide much guidance to
that endeavor and explains little on how the formalization is structured. The
related work section passes really quickly on some related topics, for instance
the large body of litterature on realizability and extraction, as well as topics
on λΠ and modal lambda calculi.

[1] `Propositions as [Types].` Steven Awodey, Andrej Bauer, J. Log. Comput. 14(4): 447-471 (2004)

Comments for authors
--------------------
* The connection between the definitions and lemmas presented in the paper and
  the formal development could be more explicitly explained, for example by
  providing the mapping between statements and formal theorems or links.

* The use of the expressions "generalized intersections and unions" (l.437)
  clashes with their use in λ-calculi with intersection points and variants of
  the Implicit Calculus of Constructions for dependent types

* Section 4.1: the direction of the upgrade contradicts the interpretation in
  terms of squash, if Γ ≤ Δ then we should morally have a squashing substitution
  Δ → Γ

* The absence of squash as a Type constructor makes the description of a few
  constructions awkward and duplicates many constructions (for instance the rule
  β_{ir} and Ir-Ty)

Additional typos:
- l. 32: "for example, For example" (fixed)
- l. 104: "there are no call in the if branch, so we generate no verification conditions" a (trivial) verification condition that 0 >= 0 should still be generated to discharge the typing of `0 : nat` (removed)
- l.144: "In our work, build a refinement type system" (fixed)
- l. 202: `mstrans_{p,q,r}` should be `trans_{p,q,r}` (fixed)
- l. 211: type equality should also allow unfolding of definitions as in the proof sketch beside α-renaming (WONTFIX)
- l. 211: the footnote should not be followed by a dot (fixed)
- l. 212: "simple proofs very long. ." (fixed)
- l. 213: "syntax sugar" should be "syntactic sugar" (ok sure)
- l. 217: Some `\hat{λ}` are missing and `Vec A n` should be `|| Vec A  n ||` (fixed)
- l. 272: "likseo" (fixed)
- l. 273: a comma is missing between `1 |b|` (fixed)
- l. 273-274: where do the multiplicative/exponential vocabular comes from ? (FIXED)
- l. 300: I would have expected the inductive step of reasoning to be a proper λ rather than an ad-hoc abstraction (WONTFIX)
- l. 319: "almost any fact about arithmetic" This statement is a bit strange. It would be more convincing to state that induction is enough to prove in your system any provable statement in some formal system of arithmetic. (FIXED: tried to avoid issue)
- l. 352: In `Γ ⊢ p : ϕ` the `ϕ` should be a `\varphi` (fixed)
- l. 374: dot missing after `Figure 3` (fixed)
- l. 393: Conversely, if Γ ≤ Δ then Δ = Γ^↑ ? (FIXED)
- l. 437: "refinment" should be "refinement" (fixed)
- l. 537: the closing parens should come before the dot (fixed. Grammar, eh?)
- l. 574: "for some valid a.." should be "for some valid a." (fixed)
- l. 554: In the conclusion of Lam-Pr, `λ u.e` should be `λ u : φ. e` (fixed)
- l. 563: In the last premise of Let-Ir, `||x|||` should be `||x||` (fixed)
- l. 565: In the last premise of Natrec `: s :` should be `⊢ s :` (fixed)
- l. 566: The conclusion of natrec should be, `Γ ⊢ natrec[n ↦ C] e z (||succ x||, y ↦ s) : [e/n]C` (missing judgement, wrong variable name for the motive `C`) (fixed)
- l. 569: The rule for introducing `() : 1` seems to be missing (fixed)
- l. 593: In the conlusion of rule MP, the type should be `[q/u]ψ` (fixed)
- l. 609-623: Some Γ^↑ do not seem necessary in the premises of the rules Let-Exists, Let-Pair-Pr, Let-Ir-Pr, Cases-Pr and Ind (because the premises are proofs and not computationally relevant terms) (fixed)
- l. 624: The conclusion of rule Ind should be `Γ ⊢ ind[n ↦ φ] e z (succ x, y ↦ s) : [e/n]φ` (fixed)
- l. 649: In the conclusion of the rule β_{ir}, the left-hand side of the equation should be `(λ ||x : A||. e) ||a||` (fixed)
- l. 703: "being an initial object" should be "being a terminal object" (at least in categories of context and substitutions) (fixed)
- l. 704: the introduction rule should be `True` and not `β_succ` (fixed)
- l. 720: "on a proof variable u for the right hand side" should be "on a proof variable for the left hand side" (fixed)
- l. 721: "where φ is not well-formed" should be "where ψ is not well-formed" (fixed)
- l. 809: `dntΓ` should be `⟦Γ⟧` (fixed)
- l. 838: In the typing rule for λ-abstraction the body of the conclusion should be `e` instead of `s` (fixed)
- l. 860: This equation do not typecheck, the left-hand side is supposed to be of type `M(⟦A⟧ → M⟦B⟧)` whereas the right hand side is of type M⟦A⟧ → M⟦B⟧ (fixed)
- l. 953: Some of the `A`s should probably be `B`s (fixed)
- l. 972: `x` should be in the (subset) interpretation of `A` (fixed)
- l. 986: the phrase does not seem terminated properly (fixed)
- l. 1021: `We otheriwse` should be `We otherwise` (fixed)
- l. 1040: `⟦Γ, ⊢ φ⟧` should be  `⟦Γ ⊢ φ⟧` (fixed)
- l. 1101: `as the fact` should probably be `is the fact` (removed)

Questions for author response
-----------------------------
* Section 3 heavily employs lists as a leading example but they are not present
  in the language (as mentioned on lines 634-636). What would be the challenges
  to extend the language with such inductive types and refinements on these ?

* What's the impact in terms of expressivity of the absence of extensionality
  rules for pairs, subsets, etc in the equality rules ? Similarly, most
  congruences of equality can be obtained through the Subst rule but I do not
  see a ξ-rule (congruence for λ-abstraction), e.g Γ, x : A ⊢ p : t = u ⇒ Γ ⊢
  ξ(x ↦ p) : λ x : A. t = λ x : A. u

* Even if the paper acknowleges explicitly the absence of higher order features,
  reasoning on higher order functions can be extremely useful. How does function
  equality work ? Can anything be proved for higher-order functions (map,
  filter, etc) ?
